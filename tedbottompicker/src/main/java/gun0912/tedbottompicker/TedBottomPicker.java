package gun0912.tedbottompicker;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import gun0912.tedbottompicker.adapter.GalleryAdapter;

public class TedBottomPicker extends BottomSheetDialogFragment {

    public static final String TAG = "TedBottomPicker";
    static final String EXTRA_CAMERA_IMAGE_URI = "camera_image_uri";
    static final String EXTRA_CAMERA_SELECTED_IMAGE_URI = "camera_selected_image_uri";
    //public static String[] gallery_path_1;
    public Builder builder;
    GalleryAdapter imageGalleryAdapter;
    View view_title_container;
    TextView tv_title;
    Button btn_done;
    FrameLayout selected_photos_container_frame;
    HorizontalScrollView hsv_selected_photos;
    LinearLayout selected_photos_container;
    TextView selected_photos_empty;
    View contentView;
    ArrayList<Uri> selectedUriList = new ArrayList<>();
    ArrayList<Uri> tempUriList = new ArrayList<>();
    private Uri cameraImageUri;
    private RecyclerView rc_gallery;
    public  ArrayList<String> al_images = new ArrayList<>();

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {


        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            Log.d(TAG, "onStateChanged() newState: " + newState);
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismissAllowingStateLoss();
            }


        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            Log.d(TAG, "onSlide() slideOffset: " + slideOffset);
        }
    };
    private String[] FilePathStrings;
    private boolean check = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        selectedUriList.clear();
        tempUriList.clear();
        //gallery_path_1 = new String[0];
        cameraImageUri = builder.selectedUri;
        tempUriList = builder.selectedUriList;
        //updateSelectedView();
        //  setRetainInstance(true);
    }

    public void show(FragmentManager fragmentManager) {

        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.add(this, getTag());
        ft.commitAllowingStateLoss();
//        ft.commit();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onViewCreated(View contentView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(contentView, savedInstanceState);


    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        contentView = View.inflate(getContext(), R.layout.tedbottompicker_content_view, null);
        dialog.setContentView(contentView);
        CoordinatorLayout.LayoutParams layoutParams =
                (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = layoutParams.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
            if (builder != null && builder.peekHeight > 0) {
                ((BottomSheetBehavior) behavior).setPeekHeight(builder.peekHeight);
            }

        }

        initView(contentView);

        setTitle();
        setRecyclerView();
        setSelectionView();

        selectedUriList = new ArrayList<>();
        selectedUriList.clear();


        if (builder.onImageSelectedListener != null && cameraImageUri != null) {
            addUri(cameraImageUri);
        } else if (builder.onMultiImageSelectedListener != null && tempUriList != null) {
            /*for (Uri uri : tempUriList) {
                addUri(uri);
            }*/
        }

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onMultiSelectComplete();
                //updateSelectedView();
                selected_photos_empty.setVisibility(View.VISIBLE);
                selected_photos_container.setVisibility(View.GONE);
                btn_done.setText(builder.completeButtonText);
                btn_done.setVisibility(View.GONE);


            }
        });
        checkMultiMode();


    }

    private void setSelectionView() {

        if (builder.emptySelectionText != null) {
            selected_photos_empty.setText(builder.emptySelectionText);
        }


    }

    /*public void setDoneButton() {

     *//*if (builder.completeButtonText != null) {
            btn_done.setText(builder.completeButtonText);
        }*//*

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onMultiSelectComplete();


            }
        });
    }*/

    public void onMultiSelectComplete() {

        builder.onMultiImageSelectedListener.onImagesSelected(selectedUriList);
        for (int k = 0; k < selectedUriList.size(); k++) {
            File f1 = new File(selectedUriList.get(k).getPath());
            Log.d("Image_Path", "----------" + f1.getAbsolutePath());
            if (f1.exists()) {
                ContentResolver contentResolver = getContext().getContentResolver();
                String canonicalPath;
                try {
                    canonicalPath = f1.getCanonicalPath();
                } catch (IOException e) {
                    canonicalPath = f1.getAbsolutePath();
                }
                final Uri uri = MediaStore.Files.getContentUri("external");
                final int result = contentResolver.delete(uri,
                        MediaStore.Files.FileColumns.DATA + "=?", new String[]{canonicalPath});
                if (result == 0) {
                    final String absolutePath = f1.getAbsolutePath();
                    if (!absolutePath.equals(canonicalPath)) {
                        contentResolver.delete(uri,
                                MediaStore.Files.FileColumns.DATA + "=?", new String[]{absolutePath});
                    }
                }

                //boolean b = f1.delete();
                check = true;

                removeImage(selectedUriList.get(k));

                //Log.d("kkkk-----3", "Check_DeleteImage------------" + checking);
                //setRecyclerView();
            }
        }

        selectedUriList.clear();
        //String directory_path_1 = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "MyOCR_Images";


        ArrayList<String> all_gallery_images = gallery_imagespath();
        // Create a String array for FilePathStrings
        FilePathStrings = new String[all_gallery_images.size()];

        for (int i = 0; i < all_gallery_images.size(); i++) {
            FilePathStrings[i] = all_gallery_images.get(i);
        }

        //gallery_path_1 = FilePathStrings;
        updateSelectedView();
        builder.gallery_path = FilePathStrings;
        builder.spacing = getActivity().getResources().getDimensionPixelSize(R.dimen.tedbottompicker_grid_layout_margin);
        setRecyclerView();

        // dismissAllowingStateLoss();
    }

    private void checkMultiMode() {
        if (!isMultiSelect()) {
            btn_done.setVisibility(View.GONE);
            selected_photos_container_frame.setVisibility(View.GONE);
        }

    }

    private void initView(View contentView) {

        view_title_container = contentView.findViewById(R.id.view_title_container);
        rc_gallery = contentView.findViewById(R.id.rc_gallery);
        tv_title = contentView.findViewById(R.id.tv_title);
        btn_done = contentView.findViewById(R.id.btn_done);

        selected_photos_container_frame = contentView.findViewById(R.id.selected_photos_container_frame);
        hsv_selected_photos = contentView.findViewById(R.id.hsv_selected_photos);
        selected_photos_container = contentView.findViewById(R.id.selected_photos_container);
        selected_photos_empty = contentView.findViewById(R.id.selected_photos_empty);

        hsv_selected_photos.setVisibility(View.VISIBLE);
    }

    private void setRecyclerView() {

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        rc_gallery.setLayoutManager(gridLayoutManager);
        rc_gallery.addItemDecoration(new GridSpacingItemDecoration(gridLayoutManager.getSpanCount(), builder.spacing, false));
        updateAdapter();
    }

    private void updateAdapter() {

        imageGalleryAdapter = new GalleryAdapter(getActivity(), builder, builder.gallery_path);
        rc_gallery.setAdapter(imageGalleryAdapter);
        imageGalleryAdapter.setOnItemClickListener(new GalleryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                GalleryAdapter.PickerTile pickerTile = imageGalleryAdapter.getItem(position);

                switch (pickerTile.getTileType()) {

                    case GalleryAdapter.PickerTile.IMAGE:
                        complete(pickerTile.getImageUri());

                        break;

                    default:
                        errorMessage();
                }

            }
        });
    }

    private void complete(final Uri uri) {
        Log.d(TAG, "selected uri: " + uri.toString());
        //uri = Uri.parse(uri.toString());
        if (isMultiSelect()) {


            if (selectedUriList.contains(uri)) {
                removeImage(uri);
            } else {
                addUri(uri);
            }


        } else {
            builder.onImageSelectedListener.onImageSelected(uri);
            dismissAllowingStateLoss();
        }

    }

    private boolean addUri(final Uri uri) {


        if (selectedUriList.size() == builder.selectMaxCount) {
            String message;
            if (builder.selectMaxCountErrorText != null) {
                message = builder.selectMaxCountErrorText;
            } else {
                message = String.format(getResources().getString(R.string.select_max_count), builder.selectMaxCount);
            }

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            return false;
        }


        selectedUriList.add(uri);

        final View rootView = LayoutInflater.from(getActivity()).inflate(R.layout.tedbottompicker_selected_item, null);
        ImageView thumbnail = rootView.findViewById(R.id.selected_photo);
        ImageView iv_close = rootView.findViewById(R.id.iv_close);
        rootView.setTag(uri);

        selected_photos_container.addView(rootView, 0);


        //int px = (int) getResources().getDimension(R.dimen.tedbottompicker_selected_image_height);
        int wdpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
        int htpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());

        thumbnail.setLayoutParams(new FrameLayout.LayoutParams(wdpx, htpx));

        if (builder.imageProvider == null) {
            Glide.with(getActivity())
                    .load(uri)
                    .thumbnail(0.1f)
                    .dontAnimate()
                    .centerCrop()
                    .placeholder(R.drawable.ic_gallery)
                    .error(R.drawable.img_error)
                    .into(thumbnail);
        } else {
            builder.imageProvider.onProvideImage(thumbnail, uri);
        }


        if (builder.deSelectIconDrawable != null) {
            iv_close.setImageDrawable(builder.deSelectIconDrawable);
        }

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checking = selectedUriList.remove(uri);
                Log.d("KKKK----1", "Is Delete?" + checking);
                removeImage(uri);
                updateSelectedView();

            }
        });


        updateSelectedView();
        imageGalleryAdapter.setSelectedUriList(selectedUriList, uri);
        return true;

    }

    private void removeImage(Uri uri) {

        if (!check) {
            boolean checking = selectedUriList.remove(uri);
            Log.d("KKKK----2", "Is Delete?" + checking);
        }
        check = false;


        for (int i = 0; i < selected_photos_container.getChildCount(); i++) {
            View childView = selected_photos_container.getChildAt(i);


            if (childView.getTag().equals(uri)) {
                selected_photos_container.removeViewAt(i);
                break;
            }
        }

        updateSelectedView();
        imageGalleryAdapter.setSelectedUriList(selectedUriList, uri);
    }

    private void updateSelectedView() {

        if (selectedUriList == null || selectedUriList.size() == 0) {
            selected_photos_empty.setVisibility(View.VISIBLE);
            selected_photos_container.setVisibility(View.GONE);
            btn_done.setText(builder.completeButtonText);
            btn_done.setVisibility(View.GONE);

        } else {
            selected_photos_empty.setVisibility(View.GONE);
            selected_photos_container.setVisibility(View.VISIBLE);
            btn_done.setText(builder.completeButtonText);
            btn_done.setVisibility(View.VISIBLE);

        }

    }


    private void errorMessage(String message) {
        String errorMessage = message == null ? "Something wrong." : message;

        if (builder.onErrorListener == null) {
            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
        } else {
            builder.onErrorListener.onError(errorMessage);
        }
    }


    private void errorMessage() {
        errorMessage(null);
    }

    private void setTitle() {

        if (!builder.showTitle) {
            tv_title.setVisibility(View.GONE);

            if (!isMultiSelect()) {
                view_title_container.setVisibility(View.GONE);
            }

            return;
        }

        if (!TextUtils.isEmpty(builder.title)) {
            tv_title.setText(builder.title);
        }

        if (builder.titleBackgroundResId > 0) {
            tv_title.setBackgroundResource(builder.titleBackgroundResId);
        }

    }

    private boolean isMultiSelect() {
        return builder.onMultiImageSelectedListener != null;
    }


    public interface OnMultiImageSelectedListener {
        void onImagesSelected(ArrayList<Uri> uriList);
    }

    public interface OnImageSelectedListener {
        void onImageSelected(Uri uri);
    }

    public interface OnErrorListener {
        void onError(String message);
    }

    public interface ImageProvider {
        void onProvideImage(ImageView imageView, Uri imageUri);
    }

    public static class Builder {

        public String[] gallery_path;
        public Context context;
        public int previewMaxCount = 25;
        public Drawable cameraTileDrawable;
        public Drawable galleryTileDrawable;

        public Drawable deSelectIconDrawable;
        public Drawable selectedForegroundDrawable;

        public int spacing = 1;
        public OnImageSelectedListener onImageSelectedListener;
        public OnMultiImageSelectedListener onMultiImageSelectedListener;
        public OnErrorListener onErrorListener;
        public ImageProvider imageProvider;
        public boolean showCamera = true;
        public boolean showGallery = true;
        public int peekHeight = -1;
        public int cameraTileBackgroundResId = R.color.tedbottompicker_camera;
        public int galleryTileBackgroundResId = R.color.tedbottompicker_gallery;

        public String title;
        public boolean showTitle = true;
        public int titleBackgroundResId;

        public int selectMaxCount = Integer.MAX_VALUE;
        public int selectMinCount = 0;


        public String completeButtonText;
        public String emptySelectionText;
        public String selectMaxCountErrorText;
        public String selectMinCountErrorText;
        /*public @MediaType
        int mediaType = MediaType.IMAGE;*/
        ArrayList<Uri> selectedUriList;
        Uri selectedUri;

        public Builder(@NonNull Context context, String[] gallery_path) {

            this.context = context;
            this.gallery_path = gallery_path;

            setSpacingResId(R.dimen.tedbottompicker_grid_layout_margin);
            //gallery_path_1 = gallery_path;
        }


        public Builder setCameraTile(Drawable cameraTileDrawable) {
            this.cameraTileDrawable = cameraTileDrawable;
            return this;
        }


        public Builder setGalleryTile(Drawable galleryTileDrawable) {
            this.galleryTileDrawable = galleryTileDrawable;
            return this;
        }

        public Builder setSpacingResId(@DimenRes int dimenResId) {
            this.spacing = context.getResources().getDimensionPixelSize(dimenResId);
            return this;
        }

        public Builder setDeSelectIcon(@DrawableRes int deSelectIconResId) {
            setDeSelectIcon(ContextCompat.getDrawable(context, deSelectIconResId));
            return this;
        }

        public Builder setDeSelectIcon(Drawable deSelectIconDrawable) {
            this.deSelectIconDrawable = deSelectIconDrawable;
            return this;
        }

        public Builder setSelectedForeground(@DrawableRes int selectedForegroundResId) {
            setSelectedForeground(ContextCompat.getDrawable(context, selectedForegroundResId));
            return this;
        }

        public Builder setSelectedForeground(Drawable selectedForegroundDrawable) {
            this.selectedForegroundDrawable = selectedForegroundDrawable;
            return this;
        }

        public Builder setPreviewMaxCount(int previewMaxCount) {
            this.previewMaxCount = previewMaxCount;
            return this;
        }

        public Builder setSelectMaxCount(int selectMaxCount) {
            this.selectMaxCount = selectMaxCount;
            return this;
        }

        public Builder setSelectMinCount(int selectMinCount) {
            this.selectMinCount = selectMinCount;
            return this;
        }

        public Builder setOnImageSelectedListener(OnImageSelectedListener onImageSelectedListener) {
            this.onImageSelectedListener = onImageSelectedListener;
            return this;
        }

        public Builder setOnMultiImageSelectedListener(OnMultiImageSelectedListener onMultiImageSelectedListener) {
            this.onMultiImageSelectedListener = onMultiImageSelectedListener;
            return this;
        }

        public Builder setOnErrorListener(OnErrorListener onErrorListener) {
            this.onErrorListener = onErrorListener;
            return this;
        }

        public Builder showCameraTile(boolean showCamera) {
            this.showCamera = showCamera;
            return this;
        }

        public Builder showGalleryTile(boolean showGallery) {
            this.showGallery = showGallery;
            return this;
        }

        public Builder setSpacing(int spacing) {
            this.spacing = spacing;
            return this;
        }

        public Builder setPeekHeight(int peekHeight) {
            this.peekHeight = peekHeight;
            return this;
        }

        public Builder setPeekHeightResId(@DimenRes int dimenResId) {
            this.peekHeight = context.getResources().getDimensionPixelSize(dimenResId);
            return this;
        }

        public Builder setCameraTileBackgroundResId(@ColorRes int colorResId) {
            this.cameraTileBackgroundResId = colorResId;
            return this;
        }

        public Builder setGalleryTileBackgroundResId(@ColorRes int colorResId) {
            this.galleryTileBackgroundResId = colorResId;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setTitle(@StringRes int stringResId) {
            this.title = context.getResources().getString(stringResId);
            return this;
        }

        public Builder showTitle(boolean showTitle) {
            this.showTitle = showTitle;
            return this;
        }

        public Builder setCompleteButtonText(String completeButtonText) {
            this.completeButtonText = completeButtonText;
            return this;
        }

        public Builder setCompleteButtonText(@StringRes int completeButtonResId) {
            this.completeButtonText = context.getResources().getString(completeButtonResId);
            return this;
        }

        public Builder setEmptySelectionText(String emptySelectionText) {
            this.emptySelectionText = emptySelectionText;
            return this;
        }

        public Builder setEmptySelectionText(@StringRes int emptySelectionResId) {
            this.emptySelectionText = context.getResources().getString(emptySelectionResId);
            return this;
        }

        public Builder setSelectMaxCountErrorText(String selectMaxCountErrorText) {
            this.selectMaxCountErrorText = selectMaxCountErrorText;
            return this;
        }

        public Builder setSelectMaxCountErrorText(@StringRes int selectMaxCountErrorResId) {
            this.selectMaxCountErrorText = context.getResources().getString(selectMaxCountErrorResId);
            return this;
        }

        public Builder setSelectMinCountErrorText(String selectMinCountErrorText) {
            this.selectMinCountErrorText = selectMinCountErrorText;
            return this;
        }

        public Builder setSelectMinCountErrorText(@StringRes int selectMinCountErrorResId) {
            this.selectMinCountErrorText = context.getResources().getString(selectMinCountErrorResId);
            return this;
        }

        public Builder setTitleBackgroundResId(@ColorRes int colorResId) {
            this.titleBackgroundResId = colorResId;
            return this;
        }

        public Builder setImageProvider(ImageProvider imageProvider) {
            this.imageProvider = imageProvider;
            return this;
        }

        public Builder setSelectedUriList(ArrayList<Uri> selectedUriList) {
            this.selectedUriList = selectedUriList;
            return this;
        }

        public Builder setSelectedUri(Uri selectedUri) {
            this.selectedUri = selectedUri;
            return this;
        }

      /*  public Builder showVideoMedia() {
            this.mediaType = MediaType.VIDEO;
            return this;
        }*/

        public TedBottomPicker create() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                    && ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                throw new RuntimeException("Missing required WRITE_EXTERNAL_STORAGE permission. Did you remember to request it first?");
            }

            if (onImageSelectedListener == null && onMultiImageSelectedListener == null) {
                throw new RuntimeException("You have to use setOnImageSelectedListener() or setOnMultiImageSelectedListener() for receive selected Uri");
            }

            TedBottomPicker customBottomSheetDialogFragment = new TedBottomPicker();

            customBottomSheetDialogFragment.builder = this;
            return customBottomSheetDialogFragment;
        }

        /*@Retention(RetentionPolicy.SOURCE)
        @IntDef({MediaType.IMAGE, MediaType.VIDEO})
        public @interface MediaType {
            int IMAGE = 1;
            int VIDEO = 2;
        }*/


    }

    public   ArrayList<String> gallery_imagespath() {
        al_images.clear();
        Uri uri;
        Cursor cursor;
        int column_index_data, column_index_folder_name;

        String absolutePathOfImage = null;
        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME};

        final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
        cursor = getContext().getContentResolver().query(uri, projection, null, null, orderBy + " DESC");

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        column_index_folder_name = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);

        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);
            Log.e("Column", absolutePathOfImage);
            Log.e("Folder", cursor.getString(column_index_folder_name));

            Log.d("KIRTIKANT", "All_ImageSize--1--" + al_images.size());
            al_images.add(absolutePathOfImage);


        }
        return al_images;
    }

}
