package com.digitalsuffixllc.ocr.textscanner.myocr.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import com.digitalsuffixllc.ocr.textscanner.myocr.R;
import com.digitalsuffixllc.ocr.textscanner.myocr.myocr_model.faq_model;

/**
 * Created by Kirtikant Patadiya on 03/02/2019.
 */
public class FAQAdapter extends RecyclerView.Adapter<FAQAdapter.FAQHolder> {
    private List<faq_model> faqList;
    private Context context;
    private Spanned text;
    private int check;

    public FAQAdapter(int check ,List<faq_model> faqList, Context context) {
        this.faqList = faqList;
        this.context = context;
        this.check = check;
    }

    @Override
    public FAQHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.faq_raw, parent, false);

        return new FAQHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FAQHolder holder, int position) {

        faq_model faqModel = faqList.get(position);
        holder.tv_faq_question.setText(faqModel.getFaq_question());
        holder.tv_faq_answer.setText(faqModel.getFaq_answer());
        holder.ll_faq_date.setVisibility(View.GONE);


        holder.iv_faq_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.ll_faq_answer.getVisibility() == View.VISIBLE) {
                    /*if(check==2){
                        holder.ll_faq_date.setVisibility(View.VISIBLE);
                          holder.tv_faq_date.setText(faqModel.getFaq_date());
                    }else{
                        holder.ll_faq_date.setVisibility(View.GONE);
                    }*/
                    holder.ll_faq_date.setVisibility(View.GONE);
                    Glide.with(context)
                            .load(R.drawable.faq_left)
                            .thumbnail(0.1f)
                            .dontAnimate()
                            .centerCrop()
                            .placeholder(R.drawable.faq_left)
                            .error(R.drawable.faq_left)
                            .into(holder.iv_faq_icon);
                    //holder.iv_faq_icon.setImageDrawable(R.drawable.faq_left);
                    holder.ll_faq_answer.setVisibility(View.GONE);
                } else {
                    if(check==2){
                        holder.ll_faq_date.setVisibility(View.VISIBLE);
                        holder.tv_faq_date.setText(faqModel.getFaq_date());
                    }else{
                        holder.ll_faq_date.setVisibility(View.GONE);
                    }
                    Glide.with(context)
                            .load(R.drawable.faq_down)
                            .thumbnail(0.1f)
                            .dontAnimate()
                            .centerCrop()
                            .placeholder(R.drawable.faq_down)
                            .error(R.drawable.faq_down)
                            .into(holder.iv_faq_icon);
                    holder.ll_faq_answer.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return faqList.size();
    }

    public class FAQHolder extends RecyclerView.ViewHolder {
        private LinearLayout ll_faq_question, ll_faq_answer, ll_faq_date;
        private TextView tv_faq_question, tv_faq_answer, tv_faq_date;
        private ImageView iv_faq_icon;

        public FAQHolder(View itemView) {
            super(itemView);
            ll_faq_question = itemView.findViewById(R.id.ll_faq_question);
            tv_faq_question = itemView.findViewById(R.id.tv_faq_question);
            iv_faq_icon = itemView.findViewById(R.id.iv_faq_icon);
            ll_faq_answer = itemView.findViewById(R.id.ll_faq_answer);
            tv_faq_answer = itemView.findViewById(R.id.tv_faq_answer);
            ll_faq_date = itemView.findViewById(R.id.ll_faq_date);
            tv_faq_date = itemView.findViewById(R.id.tv_faq_date);
        }
    }
}
