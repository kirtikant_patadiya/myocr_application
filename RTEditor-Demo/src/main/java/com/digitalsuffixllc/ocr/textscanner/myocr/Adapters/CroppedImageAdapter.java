package com.digitalsuffixllc.ocr.textscanner.myocr.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.airbnb.lottie.LottieAnimationView;

import java.io.File;

import com.digitalsuffixllc.ocr.textscanner.myocr.NewOCRProcess;
import com.digitalsuffixllc.ocr.textscanner.myocr.OCR_Process;
import com.digitalsuffixllc.ocr.textscanner.myocr.R;

/**
 * Created by KK_Android on 05-01-2018.
 */

public class CroppedImageAdapter extends RecyclerView.Adapter<CroppedImageAdapter.RecyclerViewHolder> {
    private String[] listImage;
    private Context mContext;
    private Dialog MyDialog;

    public CroppedImageAdapter(Context mContext, String[] listImage) {
        this.mContext=mContext;
        this.listImage  =listImage;

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_image_item, null);
        return new RecyclerViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, final int position) {
        File f = new File(listImage[position]);
        if (f.exists()) {

            try{
                Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
                int scale = bitmap.getHeight() * (256 / bitmap.getWidth());
                Bitmap scaled_bitmap = Bitmap.createScaledBitmap(bitmap,300,300,true);
                holder.raw_image_cropped.setImageBitmap(scaled_bitmap);
            }catch (Exception e){
                e.printStackTrace();
            }



            holder.raw_image_cropped.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   MyCustomAlertDialog(listImage[position]);

                }
            });

            holder.anm_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    File f1 = new File(listImage[position]);
                    Log.d("Image_Path","----------"+f1.getAbsolutePath());
                    if(f1.exists()) {
                        boolean b = f1.delete();
                        Log.d("Image_Path","Check_DeleteImage------------"+b);
                        notifyItemChanged(position);
                        notifyDataSetChanged();
                        OCR_Process.refreshData(mContext);
                    }

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listImage.length;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private LottieAnimationView anm_delete;
        private ImageView raw_image_cropped;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            raw_image_cropped = itemView.findViewById(R.id.raw_image_cropped);
            anm_delete = itemView.findViewById(R.id.anm_delete);
        }

        @Override
        public void onClick(View view) {

        }
    }

    public void MyCustomAlertDialog(String path){
        MyDialog = new Dialog(mContext);
        MyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        MyDialog.setContentView(R.layout.image_layout);
        MyDialog.setTitle("");
        MyDialog.setCancelable(true);

        //hello = (Button)MyDialog.findViewById(R.id.hello);
        // close = (Button)MyDialog.findViewById(R.id.close);

        // hello.setEnabled(true);
        // close.setEnabled(true);
        ImageView testImage = MyDialog.findViewById(R.id.dialogImage);
        LottieAnimationView anm_deletedialog = MyDialog.findViewById(R.id.anm_deletedialog);
        File f =  new File(path);
        if (f.exists()) {
            Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath());

            testImage.setImageBitmap(bitmap);
        }
        anm_deletedialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyDialog.dismiss();
            }
        });
        // Picasso.with(context).load(path).cachePolicy(NO_CACHE, NO_STORE).into(testImage);
        //Picasso.with(context).load(path).memoryPolicy(NO_CACHE).into(testImage);
        /*hello.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Hello, I'm Custom Alert Dialog", Toast.LENGTH_LONG).show();
            }
        });*/
//        close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                MyDialog.cancel();
//            }
//        });

        MyDialog.show();
    }
}
