package com.digitalsuffixllc.ocr.textscanner.myocr.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import com.digitalsuffixllc.ocr.textscanner.myocr.R;
import com.digitalsuffixllc.ocr.textscanner.myocr.RTEditorActivity;


public class RTF_File_Adapter extends BaseAdapter {

    Context mContext;
    List<String> filesList;
    //String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "MyOCR_Images" + File.separator + "MyFiles/";
    String dirPath;
    private SparseBooleanArray mSelectedItemsIds;
    //private SwipeLayout ll_rtfmain;
    private TextView rowTextView;
    private HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();

    public RTF_File_Adapter(Context mContext, List<String> filesList) {
        this.mContext = mContext;
        this.filesList = filesList;
        mSelectedItemsIds = new SparseBooleanArray();
        dirPath = mContext.getExternalCacheDir().getAbsolutePath() + File.separator + "MyOCR_Images" + File.separator + "MyFiles/";
    }

    public void setNewSelection(int position, boolean value) {
        mSelection.put(position, value);
    }

    public boolean isPositionChecked(int position) {
        Boolean result = mSelection.get(position);
        return result == null ? false : result;
    }

    public Set<Integer> getCurrentCheckedPosition() {
        return mSelection.keySet();
    }

    public void removeSelection(int position) {
        mSelection.remove(position);
    }

    public void clearSelection() {
        mSelection = new HashMap<Integer, Boolean>();
    }

    @Override
    public int getCount() {
        return filesList.size();
    }

    @Override
    public Object getItem(int position) {
        return filesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.singleraw_layout, parent, false);

        itemView.setBackgroundColor(mContext.getResources().getColor(R.color.white)); //default color

        if (mSelection.get(position) != null) {
            itemView.setBackgroundColor(mContext.getResources().getColor(R.color.normal_item
            ));// this is a selected position so make it red
        }

        //ll_rtfmain = itemView.findViewById(R.id.ll_rtfmain);
        rowTextView = itemView.findViewById(R.id.rowTextView);

        rowTextView.setText(filesList.get(position));
        //ll_rtfmain.setShowMode(SwipeLayout.ShowMode.PullOut);
        //View starBottView = ll_rtfmain.findViewById(R.id.starbott);
        //ll_rtfmain.addDrag(SwipeLayout.DragEdge.Left, ll_rtfmain.findViewById(R.id.bottom_wrapper));
        //ll_rtfmain.addDrag(SwipeLayout.DragEdge.Right, ll_rtfmain.findViewById(R.id.bottom_wrapper_2));
        //ll_rtfmain.addDrag(SwipeLayout.DragEdge.Top, starBottView);
        //ll_rtfmain.addDrag(SwipeLayout.DragEdge.Bottom, starBottView);

        //TODO--Change feature of Rename & Delete
        /*ll_rtfmain.findViewById(R.id.star2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext, "Star" + position, Toast.LENGTH_SHORT).show();
                RenameDirectory(filesList.get(position));
            }
        });

        ll_rtfmain.findViewById(R.id.trash2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(mContext, "Trash Bin" + position, Toast.LENGTH_SHORT).show();
                DeleteDirectory(filesList.get(position));
            }
        });*/
//TODO--Change feature of Rename & Delete

        /*ll_rtfmain.addRevealListener(R.id.starbott, new SwipeLayout.OnRevealListener() {
            @Override
            public void onReveal(View child, SwipeLayout.DragEdge edge, float fraction, int distance) {
                View star = child.findViewById(R.id.star);
                float d = child.getHeight() / 2 - star.getHeight() / 2;
                ViewHelper.setTranslationY(star, d * fraction);
                ViewHelper.setScaleX(star, fraction + 0.6f);
                ViewHelper.setScaleY(star, fraction + 0.6f);
            }
        });*/

        return itemView;
    }

    private void DeleteDirectory(String filename) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                mContext);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setTitle("Are you sure to delete :");

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @SuppressLint("NewApi")
                            public void onClick(DialogInterface dialog, int id) {
                                if (filename.contains(".pdf")) {
                                    try {

                                        // delete the original file
                                        new File(dirPath + "PDF_Collection" + File.separator + filename).delete();
                                    } catch (Exception e) {
                                        Log.e("tag", e.getMessage());
                                    }
                                    //TODO----Refresh PDF File

                                    //final String dirPath_1 = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "MyOCR_Images" + File.separator + "MyFiles" + File.separator + "PDF_Collection/";
                                    final String dirPath_1 = mContext.getExternalCacheDir().getAbsolutePath() + File.separator + "MyOCR_Images" + File.separator + "MyFiles" + File.separator + "PDF_Collection/";
                                    File dir_1 = new File(dirPath_1);
                                    if (!dir_1.exists()) {
                                        dir_1.mkdir();
                                    }
                                    File[] m_Filelist = dir_1.listFiles();
                                    Arrays.sort(m_Filelist, Collections.reverseOrder());
                                    if (m_Filelist.length > 0) {
                                        RTEditorActivity.mainListView.setVisibility(View.VISIBLE);
                                        //RTEditorActivity.ll_mainListview.setVisibility(View.VISIBLE);
                                        RTEditorActivity.editorr.setVisibility(View.GONE);
                                        //new RTEditorActivity().getSupportActionBar().hide();
                                        try {
                                            new RTEditorActivity().getSupportActionBar().hide();
                                        } catch (Exception e) {

                                        }
                                        /*if (m_Filelist != null && m_Filelist.length > 1) {
                                            Arrays.sort(m_Filelist, new Comparator<File>() {
                                                @Override
                                                public int compare(File object1, File object2) {
                                                    return (int) ((object1.lastModified() > object2.lastModified()) ? object1.lastModified(): object2.lastModified());
                                                }
                                            });
                                        }*/

                                    /*File dir_2 = new File(dirPath_1);
                                    if (!dir_2.exists()) {
                                        dir_2.mkdir();
                                    }
                                    File[] m_filelist = dir_2.listFiles();*/

                                        final String[] theNamesOfFilesToShow = new String[m_Filelist.length];
                                        int k = 0;


                                        for (int i = 0; i < m_Filelist.length; i++) {
                                            theNamesOfFilesToShow[i] = m_Filelist[i].getName();
                                        }
                                        List<String> FilesList1 = new ArrayList<String>();
                                        FilesList1.addAll(Arrays.asList(theNamesOfFilesToShow));
                                        filesList.clear();
                                        filesList = FilesList1;
                                        RTEditorActivity.filelist_pdf = m_Filelist;
                                        //RTEditorActivity.mainListView.setVisibility(View.GONE);
                                        //RTEditorActivity.editorr.setVisibility(View.VISIBLE);
                                        //((Activity)mContext).getSupportActionBar().show();
                                        RTEditorActivity.mainListView.setAdapter(new RTF_File_Adapter(mContext, filesList));
                                        notifyDataSetChanged();

                                        //TODO----Refresh PDF Files
                                    } else {
                                        RTEditorActivity.mainListView.setVisibility(View.GONE);
                                        //RTEditorActivity.ll_mainListview.setVisibility(View.GONE);
                                        RTEditorActivity.editorr.setVisibility(View.VISIBLE);
                                        try {
                                            new RTEditorActivity().getSupportActionBar().hide();
                                        } catch (Exception e) {

                                        }
                                        RTEditorActivity.mRTMessageField.setText("");
                                    }
                                } else {
                                    try {

                                        // delete the original file
                                        new File(dirPath + "RTF_Collection" + File.separator + filename).delete();
                                        //new File(dirPath + "TEXT_Collection" + File.separator + filename).delete();

                                    } catch (Exception e) {
                                        Log.e("tag", e.getMessage());
                                    }
                                    //TODO----Refresh RTF File

                                    //final String dirPath_1 = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "MyOCR_Images" + File.separator + "MyFiles" + File.separator + "RTF_Collection/";
                                    final String dirPath_1 = mContext.getExternalCacheDir().getAbsolutePath() + File.separator + "MyOCR_Images" + File.separator + "MyFiles" + File.separator + "RTF_Collection/";
                                    //final String dirPath_1 = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "MyOCR_Images" + File.separator + "MyFiles" + File.separator + "TEXT_Collection/";
                                    File dir_1 = new File(dirPath_1);
                                    if (!dir_1.exists()) {
                                        dir_1.mkdir();
                                    }
                                    File[] m_filelist = dir_1.listFiles();
                                    Arrays.sort(m_filelist, Collections.reverseOrder());
                                    if (m_filelist.length > 0) {
                                        RTEditorActivity.mainListView.setVisibility(View.VISIBLE);
                                        //RTEditorActivity.ll_mainListview.setVisibility(View.VISIBLE);
                                        RTEditorActivity.editorr.setVisibility(View.GONE);
                                        try {
                                            new RTEditorActivity().getSupportActionBar().hide();
                                        } catch (Exception e) {

                                        }


                                        //Arrays.sort(m_filelist, Comparator.comparingLong(File::lastModified).reversed());
                                        /*if (m_filelist != null && m_filelist.length > 1) {
                                            Arrays.sort(m_filelist, new Comparator<File>() {
                                                @Override
                                                public int compare(File object1, File object2) {
                                                    return (int) ((object1.lastModified() > object2.lastModified()) ? object1.lastModified(): object2.lastModified());
                                                }
                                            });
                                        }*/

                                        final String[] theNamesOfFilesToShow = new String[m_filelist.length];
                                        int k = 0;
                                        for (int i = 0; i < m_filelist.length; i++) {
                                            theNamesOfFilesToShow[i] = m_filelist[i].getName();
                                        }
                                        List<String> FilesList1 = new ArrayList<String>();
                                        FilesList1.addAll(Arrays.asList(theNamesOfFilesToShow));
                                        filesList.clear();
                                        filesList = FilesList1;
                                        RTEditorActivity.filelist_rtf = m_filelist;
                                        RTEditorActivity.mainListView.setAdapter(new RTF_File_Adapter(mContext, filesList));
                                        notifyDataSetChanged();

                                        //TODO----Refresh RTF Files
                                    } else {
                                        RTEditorActivity.mainListView.setVisibility(View.GONE);
                                        //RTEditorActivity.ll_mainListview.setVisibility(View.GONE);
                                        RTEditorActivity.editorr.setVisibility(View.VISIBLE);
                                        try {
                                            new RTEditorActivity().getSupportActionBar().hide();
                                        } catch (Exception e) {

                                        }
                                        RTEditorActivity.mRTMessageField.setText("");
                                    }

                                }
                                //deleteFile(dirPath_1, filename);


                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it

        alertDialog.show();

    }

    public void RenameDirectory(String filename) {
        final String[] newName1 = new String[1];
        LayoutInflater li = LayoutInflater.from(mContext);
        View promptsView = li.inflate(R.layout.prompts, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = promptsView.findViewById(R.id.editTextDialogUserInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @SuppressLint("NewApi")
                            public void onClick(DialogInterface dialog, int id) {

                                newName1[0] = userInput.getText().toString();

                                if (!((userInput.getText().toString()).matches("[A-Za-z]*")) && !Pattern.compile("^[A-Za-z]").matcher(userInput.getText().toString()).find() && userInput.getText().toString().contains(".")) {
                                    if (userInput.getText().toString().contains(" ")) {
                                        userInput.setError("Invalid Name");
                                        //Toast.makeText(mContext, "Invalid File Name", Toast.LENGTH_LONG).show();
                                        Toast toast = Toast.makeText(mContext, "Invalid File Name", Toast.LENGTH_LONG);
                                        TextView t_view = toast.getView().findViewById(android.R.id.message);
                                        t_view.setTextColor(mContext.getResources().getColor(R.color.red));
                                        toast.show();
                                    } else {
                                        String FilePath, FilePathOLD;
                                        if (filename.contains(".pdf")) {
                                            FilePath = dirPath + "PDF_Collection" + File.separator + newName1[0] + ".pdf";
                                            FilePathOLD = dirPath + "PDF_Collection" + File.separator + filename;

                                            File from = new File(FilePathOLD);
                                            File to = new File(FilePath);
                                            if (!to.exists()) {
                                                boolean check = from.renameTo(to);
                                                //TODO----Refresh PDF File

                                                //final String dirPath_1 = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "MyOCR_Images" + File.separator + "MyFiles" + File.separator + "PDF_Collection/";
                                                final String dirPath_1 = mContext.getExternalCacheDir().getAbsolutePath() + File.separator + "MyOCR_Images" + File.separator + "MyFiles" + File.separator + "PDF_Collection/";
                                                File dir_1 = new File(dirPath_1);
                                                if (!dir_1.exists()) {
                                                    dir_1.mkdir();
                                                }
                                                File[] m_filelist = dir_1.listFiles();
                                                Arrays.sort(m_filelist, Collections.reverseOrder());
                                                // Arrays.sort(m_filelist, Comparator.comparingLong(File::lastModified).reversed());
                                            /*if (m_filelist != null && m_filelist.length > 1) {
                                                Arrays.sort(m_filelist, new Comparator<File>() {
                                                    @Override
                                                    public int compare(File object1, File object2) {
                                                        return (int) ((object1.lastModified() > object2.lastModified()) ? object1.lastModified(): object2.lastModified());
                                                    }
                                                });
                                            }*/

                                                final String[] theNamesOfFilesToShow = new String[m_filelist.length];
                                                for (int i = 0; i < m_filelist.length; i++) {
                                                    theNamesOfFilesToShow[i] = m_filelist[i].getName();
                                                }
                                                List<String> FilesList1 = new ArrayList<String>();
                                                FilesList1.addAll(Arrays.asList(theNamesOfFilesToShow));
                                                filesList.clear();
                                                filesList = FilesList1;
                                                RTEditorActivity.filelist_pdf = m_filelist;
                                                RTEditorActivity.mainListView.setAdapter(new RTF_File_Adapter(mContext, filesList));
                                                notifyDataSetChanged();

                                                //TODO----Refresh PDF Files
                                            } else {
                                                Toast toast = Toast.makeText(mContext, "File with same name already exists", Toast.LENGTH_SHORT);
                                                TextView t_view = toast.getView().findViewById(android.R.id.message);
                                                t_view.setTextColor(mContext.getResources().getColor(R.color.red));
                                                toast.show();
                                            }


                                        } else {

                                            //FilePath = dirPath + "RTF_Collection" + File.separator + newName1[0] + ".rtf";
                                            FilePath = dirPath + "RTF_Collection" + File.separator + newName1[0] + ".rtf";
                                            FilePathOLD = dirPath + "RTF_Collection" + File.separator + filename;

                                        /*FilePath = dirPath + "TEXT_Collection" + File.separator + newName1[0] + ".txt";
                                        FilePathOLD = dirPath + "TEXT_Collection" + File.separator + filename;*/

                                            File from = new File(FilePathOLD);
                                            File to = new File(FilePath);
                                            if (!to.exists()) {
                                                boolean check = from.renameTo(to);
                                                //TODO----Refresh RTF File

                                                //final String dirPath_1 = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "MyOCR_Images" + File.separator + "MyFiles" + File.separator + "RTF_Collection/";
                                                final String dirPath_1 = mContext.getExternalCacheDir().getAbsolutePath() + File.separator + "MyOCR_Images" + File.separator + "MyFiles" + File.separator + "RTF_Collection/";
                                                //final String dirPath_1 = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "MyOCR_Images" + File.separator + "MyFiles" + File.separator + "TEXT_Collection/";
                                                File dir_1 = new File(dirPath_1);
                                                if (!dir_1.exists()) {
                                                    dir_1.mkdir();
                                                }
                                                File[] m_filelist = dir_1.listFiles();
                                                Arrays.sort(m_filelist, Collections.reverseOrder());
                                                // Arrays.sort(m_filelist, Comparator.comparingLong(File::lastModified).reversed());
                                            /*if (m_filelist != null && m_filelist.length > 1) {
                                                Arrays.sort(m_filelist, new Comparator<File>() {
                                                    @Override
                                                    public int compare(File object1, File object2) {
                                                        return (int) ((object1.lastModified() > object2.lastModified()) ? object1.lastModified(): object2.lastModified());
                                                    }
                                                });
                                            }*/


                                                final String[] theNamesOfFilesToShow = new String[m_filelist.length];
                                                int k = 0;
                                                for (int i = 0; i < m_filelist.length; i++) {
                                                    theNamesOfFilesToShow[i] = m_filelist[i].getName();
                                                }
                                                List<String> FilesList1 = new ArrayList<String>();
                                                FilesList1.addAll(Arrays.asList(theNamesOfFilesToShow));
                                                filesList.clear();
                                                filesList = FilesList1;
                                                RTEditorActivity.filelist_rtf = m_filelist;
                                                RTEditorActivity.mainListView.setAdapter(new RTF_File_Adapter(mContext, filesList));
                                                notifyDataSetChanged();

                                                //TODO----Refresh RTF Files
                                            } else {
                                                Toast toast = Toast.makeText(mContext, "File with same name already exists", Toast.LENGTH_SHORT);
                                                TextView t_view = toast.getView().findViewById(android.R.id.message);
                                                t_view.setTextColor(mContext.getResources().getColor(R.color.red));
                                                toast.show();
                                            }

                                        }
                                    }
                                } else {
                                    userInput.setError("Invalid Name");
                                    //Toast.makeText(mContext, "Invalid File Name", Toast.LENGTH_LONG).show();
                                    Toast toast = Toast.makeText(mContext, "Invalid File Name", Toast.LENGTH_LONG);
                                    TextView t_view = toast.getView().findViewById(android.R.id.message);
                                    t_view.setTextColor(mContext.getResources().getColor(R.color.red));
                                    toast.show();
                                }


                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

}
