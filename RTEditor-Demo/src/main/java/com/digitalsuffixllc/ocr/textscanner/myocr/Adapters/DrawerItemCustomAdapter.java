package com.digitalsuffixllc.ocr.textscanner.myocr.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitalsuffixllc.ocr.textscanner.myocr.R;
import com.digitalsuffixllc.ocr.textscanner.myocr.myocr_model.DataModel;

/**
 * Created by anupamchugh on 10/12/15.
 */
//public class DrawerItemCustomAdapter extends ArrayAdapter<DataModel> {
public class DrawerItemCustomAdapter extends BaseAdapter {

    Context mContext;
    int layoutResourceId;
    DataModel data[] = null;

    public DrawerItemCustomAdapter(Context mContext, int layoutResourceId, DataModel[] data) {

        //super(mContext, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int position) {
        return data[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            //convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_popup, null);
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        DataModel folder = data[position];


        holder.ivImage.setImageResource(folder.icon);
        holder.tvTitle.setText(folder.name);

        if(position==0){
            convertView.setBackgroundColor(mContext.getResources().getColor(R.color.selected_item));

        }

        return convertView;

        /*View listItem = convertView;

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        listItem = inflater.inflate(layoutResourceId, parent, false);

        ImageView imageViewIcon = (ImageView) listItem.findViewById(R.id.imageViewIcon);
        TextView textViewName = (TextView) listItem.findViewById(R.id.textViewName);
*/

    }

    static class ViewHolder {
        TextView tvTitle;
        ImageView ivImage;

        ViewHolder(View view) {
            tvTitle = view.findViewById(R.id.textViewName);
            ivImage = view.findViewById(R.id.imageViewIcon);
        }
    }
}

